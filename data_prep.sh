#!/bin/bash

echo Input timeframe:
read timeframe
echo Input symbol:
read symbol
echo Input start year
read startYear
echo Input start month
read startMonth


baseUrl=https://data.binance.vision
currentYear=`date +'%Y'`

folderDir=$symbol-$timeframe-data
mkdir $folderDir

while [ $startYear -le $currentYear ]
do
  while [ $startMonth -le 12 ]
    do
        if [ $startMonth -le 9 ]
        then
            strMonth=0$startMonth
        else
            strMonth=$startMonth
        fi
        wget "https://data.binance.vision/data/spot/monthly/klines/$symbol/$timeframe/$symbol-$timeframe-$startYear-$strMonth.zip" -P $folderDir
        startMonth=$(( $startMonth + 1 ))
    done
    startMonth=1
  startYear=$(( $startYear + 1 ))
done

cd $folderDir
unzip '*.zip'
rm *.zip

cd ..

python3 concat_csv.py $folderDir