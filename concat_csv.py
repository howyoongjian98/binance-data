import pandas as pd
import glob
import sys
import datetime

path = sys.argv[1]  # use your path
all_files = glob.glob(path + "/*.csv")

li = []

for filename in all_files:
    df = pd.read_csv(filename, index_col=False, header=None, names=['Open time', 'Open', 'High', 'Low', 'Close', 'Volume',
                     'Close time', 'Quote asset volume', 'Number of trades', 'Taker buy base asset volume', 'Taker buy quote asset volume'])
    li.append(df)

df = pd.concat(li)

def convert_to_dt(dt):
    return datetime.datetime.fromtimestamp(int(dt)/1000)

df.sort_values(ascending=True, inplace=True, by='Open time')
for i, row in df.iterrows():
    df.at[i, 'Parsed Open time'] = convert_to_dt(df.iloc[i]['Open time'])

print(df.iloc[0, :])
print(df.iloc[-1, :])


df.to_csv(f'{path}.csv', index=False)